package tech.master.cartaosubscriber.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tech.master.cartaosubscriber.dtos.Log;
import tech.master.cartaosubscriber.models.LogBrunoMarcella;
import tech.master.cartaosubscriber.repositories.LogBrunoMarcellaRepository;

@Component
public class ProjetoCartaoService {
	
	@Autowired
	private LogBrunoMarcellaRepository logBrunoMarcellaRepository;
	
	@KafkaListener(id="bcmmconsumer", topics="bruno-marcella")
	public void gravarLog(@Payload String str) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		Log log = mapper.readValue(str, Log.class);
		
		LogBrunoMarcella logBrunoMarcella = new LogBrunoMarcella();
		
		logBrunoMarcella.setData(log.getData());
		logBrunoMarcella.setTime(log.getTime());
		logBrunoMarcella.setDescricao(log.getDescricao());
		
		logBrunoMarcellaRepository.save(logBrunoMarcella);
	}
}
