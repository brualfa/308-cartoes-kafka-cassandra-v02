package tech.master.cartaosubscriber.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.master.cartaosubscriber.models.LogBrunoMarcella;

public interface LogBrunoMarcellaRepository extends CrudRepository<LogBrunoMarcella, String>{

}
