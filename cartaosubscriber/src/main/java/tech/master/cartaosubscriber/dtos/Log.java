package tech.master.cartaosubscriber.dtos;

import java.time.LocalTime;

import com.datastax.driver.core.LocalDate;

public class Log {
	private String servico;
	private LocalDate data;
	private LocalTime time;
	private String descricao;
	
	public String getServico() {
		return servico;
	}
	public void setServico(String servico) {
		this.servico = servico;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
