package tech.master.cartaosubscriber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartaosubscriberApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartaosubscriberApplication.class, args);
	}

}
