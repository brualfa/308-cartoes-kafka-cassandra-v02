package tech.master.cliente;

import java.util.Optional;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ClienteService {
  @Autowired
  private ClienteRepository clienteRepository;
  @Autowired
  private UsuarioClient usuarioClient;
  @Autowired
  private KafkaTemplate<String, String> template;
  
  public Cliente criar(Usuario usuario) throws JsonProcessingException {
    usuarioClient.criar(usuario);
    
    Cliente cliente = new Cliente();
    cliente.setNome(usuario.getNome());
    cliente.setCpf(usuario.getCpf());
    
	Log log = new Log();
	log.setDescricao("Cliente cadastrado com sucesso!");
	ObjectMapper mapper = new ObjectMapper();
	template.send(new ProducerRecord<String, String>("bruno-marcella","bcmmconsumer", mapper.writeValueAsString(log)));
	
    return clienteRepository.save(cliente);
  }
  
  public Optional<Cliente> buscar(int id) {
    return clienteRepository.findById(id);
  }
  
  public Optional<Cliente> buscarPorCpf(String cpf) {
    return clienteRepository.findByCpf(cpf);
  }
}
