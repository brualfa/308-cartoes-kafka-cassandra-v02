package tech.master.lancamento;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

@RestController	
public class LancamentoController {

	@Autowired
	private LancamentoService lancamentoService;
	
	@PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
	public Lancamento criar(@Valid @RequestBody Lancamento lancamento) throws JsonProcessingException {
		return lancamentoService.criar(lancamento);
	}
	
	@GetMapping("{numero}")
	public Iterable<Lancamento> buscar(@PathVariable String numero)
	{
		return lancamentoService.buscarTodasPorNumeroDoCartao(numero);
	}
}
