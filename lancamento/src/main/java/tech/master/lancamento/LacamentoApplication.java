package tech.master.lancamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCircuitBreaker
@EnableFeignClients
public class LacamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LacamentoApplication.class, args);
	}

}
